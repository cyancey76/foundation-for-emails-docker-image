#Download base image node 10.16.3
FROM node:10.16.3

LABEL maintainer="cyancey76@gmail.com"

# Install lftp and rsync for deploying files
RUN apt-get update -qq && apt-get install -y -qq && apt-get install -y -qq lftp && apt-get install -y -qq rsync

# Install Foundation for Emails and its dependencies
RUN npm install --global foundation-cli

COPY package.json package.json
COPY gulpfile.babel.js gulpfile.babel.js

RUN npm install
